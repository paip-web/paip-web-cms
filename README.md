# PAiP Web CMS

PAiP Web CMS Project

## About
This is project of our own CMS written from ground up.

## Destination
PAiP Web CMS is project of CMS which will be available in some technologies.
### Frontend:
1. React, Redux, TypeScript
2. PHP SSR (Propably using SSR as a Service [PAiP Web PreRenderer Project])
3. Python+Django SSR
4. Node.js + Next.js (or alternative idk for now)
### Backend:
One thing for now what i will just try idk is it will be in any final version is GraphQL.
1. PHP7, PAiP Web Framework PHP, GraphQL
2. Python >=3.7, Django 2
3. Node.js, Express
### Database:
1. SQLite
2. MySQL
3. PostgreSQL
4. MongoDB
5. (Maybe) Microsoft SQL Server

## Features
1. Modules [Full Modules like Shop Module, Blog Module, Info Site Module, Elearning Module]
2. Plugins [Tools and useful things or it can be like modules but with small features not like modules which is a big package of them]
3. Basic CMS [Main Page, Pages, Content Writer Panel, Administrator Panel, Developer Panel, User Panel]

## Concept of Panels
1. User Panel - Panel with information about user with things like orders, information, deleting account and things like that
2. Role Panel - Panel with features to one role (like Content Writers, Moderators and things like that)
3. Administrator Panel - Panel with administration features for changing website
4. Developer Panel - Panel with Developer Features and Module Instalator and other things like that.